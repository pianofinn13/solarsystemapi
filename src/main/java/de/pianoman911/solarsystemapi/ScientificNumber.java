package de.pianoman911.solarsystemapi;

public record ScientificNumber(double factor, int exponent) {

    public double value() {
        return factor * Math.pow(10, exponent);
    }
}
