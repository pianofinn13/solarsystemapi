package de.pianoman911.solarsystemapi;

import de.pianoman911.solarsystemapi.calculations.nasahorizion.JPLManager;
import de.pianoman911.solarsystemapi.calculations.nasahorizion.MajorObjectInfo;
import de.pianoman911.solarsystemapi.calculations.nasahorizion.MajorObjectParser;
import de.pianoman911.solarsystemapi.objects.Body;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BodyRegistry {

    private final Map<String, Body> bodies = new HashMap<>();
    private final Map<String, MajorObjectInfo> majorObjects = new HashMap<>();

    public void registerBody(Body body) {
        bodies.put(body.rel(), body);
    }

    public void registerMajorObject(MajorObjectInfo majorObject) {
        majorObjects.put(majorObject.name().toLowerCase(), majorObject);
    }

    public Body body(String rel) {
        return bodies.get(rel);
    }

    public @Nullable Body search(String name) {
        return search(name, false);
    }

    public @Nullable MajorObjectInfo searchMajorObject(String name, boolean exact) {
        for (Map.Entry<String, MajorObjectInfo> object : majorObjects.entrySet()) {
            if (exact) {
                if (object.getKey().equalsIgnoreCase(name)) {
                    return object.getValue();
                }
            } else {
                if (object.getKey().contains(name.toLowerCase())) {
                    return object.getValue();
                }
            }
        }
        return null;
    }

    public @Nullable Body search(String name, boolean exact) {
        for (Body body : bodies.values()) {
            if (exact) {
                if (body.name().equalsIgnoreCase(name)) {
                    return body;
                } else if (body.englishName().equalsIgnoreCase(name)) {
                    return body;
                } else if (body.alternativeName().equalsIgnoreCase(name)) {
                    return body;
                }
            } else {
                if (body.name().toLowerCase().contains(name.toLowerCase())) {
                    return body;
                } else if (body.englishName().toLowerCase().contains(name.toLowerCase())) {
                    return body;
                } else if (body.alternativeName().toLowerCase().contains(name.toLowerCase())) {
                    return body;
                }
            }
        }
        return null;
    }

    public Set<Body> bodies() {
        return Set.copyOf(bodies.values());
    }
}
