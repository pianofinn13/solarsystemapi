package de.pianoman911.solarsystemapi.calculations;

import de.pianoman911.solarsystemapi.BodyType;
import de.pianoman911.solarsystemapi.ScientificNumber;
import de.pianoman911.solarsystemapi.SolarApi;
import de.pianoman911.solarsystemapi.objects.Body;
import de.pianoman911.solarsystemapi.util.SolarSystemLocation;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class OrbitalCalculator extends AbstractCalculator {

    // http://www.stjarnhimlen.se/comp/ppcomp.html
    // http://www.stjarnhimlen.se/comp/tutorial.html

    public static final double AU = 149597870700.0;

    private static final Date YEAR_2000 = new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime();
    private final Body body;
    private final SolarApi solarApi;
    private Double rs;
    private SolarSystemLocation earthLocation;
    private SolarSystemLocation sunLocation;
    private SolarSystemLocation helioCentric;
    private SolarSystemLocation eclipticCentric;
    private SolarSystemLocation geoCentric;
    private SolarSystemLocation equatorial;

    public OrbitalCalculator(Body body, SolarApi solarApi) {
        this.body = body;
        this.solarApi = solarApi;
    }

    public double rs(SolarApi solarApi) {
        if (rs == null) {
            OrbitalCalculator sun = new OrbitalCalculator(solarApi.bodyRegistry().search("Sun", true), solarApi);
            sun.calculateSun();
            rs = sun.rs;
            sunLocation = sun.helioCentric;
        }
        return rs;
    }

    public SolarSystemLocation sunLocation(SolarApi solarApi) {
        rs(solarApi);
        return sunLocation;
    }

    public SolarSystemLocation earthLocation(SolarApi solarApi) {
        if (earthLocation == null) {
            OrbitalCalculator earth = new OrbitalCalculator(solarApi.bodyRegistry().search("Earth", true), solarApi);
            earth.calculatePlanetsAndMoons();
            earthLocation = earth.geoCentric;
        }
        return earthLocation;
    }

    public void date(Date date) {
        this.date = date;
        rs = null;
        earthLocation = null;
        sunLocation = null;
        sunLocation(solarApi);
        earthLocation(solarApi);
    }

    @SuppressWarnings("all")
    protected double d(int year, int month, int day, double hours) {
        return (367 * year - 7 * (year + (month + 9) / 12) / 4 - 3 * ((year + (month - 9) / 7) / 100 + 1) / 4 + 275 * month / 9 + day - 730515) + (hours / 24.0);
    }

    public double d() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        double hours = calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) / 60.0 + calendar.get(Calendar.SECOND) / 3600.0 + calendar.get(Calendar.MILLISECOND) / 3600000.0;

        return d(year, month, day, hours);
    }

    protected double N() {
        return (body.longAscNode() * d()) % 360;
    }

    protected double i() {
        return (body.inclination() * (body.bodyType() == BodyType.PLANET ? d() : 1)) % 360;
    }

    protected double w() {
        return (body.argPeriapsis() * d()) % 360;
    }

    protected double a() {
        return body.semimajorAxis() / AU;
    }

    protected double e() {
        return body.eccentricity() * (body.bodyType() == BodyType.PLANET ? d() : 1);
    }

    protected double M() {
        return (body.meanAnomaly() * d()) % 360;
    }

    protected double w1() {
        return N() + w();
    }

    protected double L() {
        return M() + w1();
    }

    protected double q() {
        return body.perihelion();
    }

    protected double Q() {
        return body.aphelion();
    }

    protected double P() {
        return Math.pow(a(), 1.5);
    }

    protected double T() {
        return epoch() - (Math.toDegrees(M() / 360.0) / P());
    }

    protected double v() {
        return body.argPeriapsis();
    }

    protected double epoch() {
        return body.sideralOrbit();
    }

    protected double ecl() {
        return 23.4393 - new ScientificNumber(3.563, -7).value() * d();
    }

    public void calculateSun() {
        double E = M() + e() * (180 / Math.PI) * Math.sin(M()) * (1.0 + e() * Math.cos(M()));

        double xv = Math.cos(E) - e();
        double yv = Math.sqrt(1 - e() * e()) * Math.sin(E);

        double v = Math.atan2(yv, xv);
        double r = Math.sqrt(xv * xv + yv * yv);
        rs = r;

        double longitude = v + w();

        double xs = r * Math.cos(longitude);
        double ys = r * Math.sin(longitude);

        double xe = xs;
        double ye = ys * Math.cos(ecl());
        double ze = ys * Math.sin(ecl());

        double RA = Math.atan2(ye, xe);
        double Dec = Math.atan2(ze, Math.sqrt(xe * xe + ye * ye));

        helioCentric = new SolarSystemLocation(RA, Dec, r);
    }

    public double E() {
        System.out.println("N: " + N());
        System.out.println("i: " + i());
        System.out.println("w: " + w());
        System.out.println("a: " + a());
        System.out.println("e: " + e());
        System.out.println("M: " + M());
        System.out.println("d: " + d());
        System.out.println("LOL: " + (body.eccentricity() * d()));

        double E0 = M() + e() * (180 / Math.PI) * Math.sin(M()) * (1.0 + e() * Math.cos(M()));
        System.out.println("E0: " + E0);

        return E0;
    }

    public void calculatePlanetsAndMoons() {
        System.out.println("E: " + E());
        double xv = a() * (Math.cos(E()) - e());
        double yv = a() * Math.sqrt(1 - e() * e()) * Math.sin(E());

        System.out.println("xv: " + xv);
        System.out.println("yv: " + yv);

        double v = Math.atan2(yv, xv);
        double r = Math.sqrt(xv * xv + yv * yv);

        System.out.println("v: " + v);
        System.out.println("r: " + r);

        double xh = r * (Math.cos(N()) * Math.cos(v + w()) - Math.sin(N()) * Math.sin(v + w()) * Math.cos(i()));
        double yh = r * (Math.sin(N()) * Math.cos(v + w()) + Math.cos(N()) * Math.sin(v + w()) * Math.cos(i()));
        double zh = r * (Math.sin(v + w()) * Math.sin(i()));

        System.out.println("xh: " + xh);
        System.out.println("yh: " + yh);
        System.out.println("zh: " + zh);

        SolarSystemLocation relativeTo = sunLocation(solarApi);

        double longitude = Math.atan2(yh, xh);
        double latitude = Math.atan2(zh, Math.sqrt(xh * xh + yh * yh));

        if (body.englishName().equals("Pluto")) {
            double S = 50.03 + 0.033459652 * d();
            double P = 238.95 + 0.003968789 * d();

            longitude = 238.9508 + 0.00400703 * d()
                - 19.799 * Math.sin(P) + 19.848 * Math.cos(P)
                + 0.897 * Math.sin(2 * P) - 4.956 * Math.cos(2 * P)
                + 0.610 * Math.sin(3 * P) + 1.211 * Math.cos(3 * P)
                - 0.341 * Math.sin(4 * P) - 0.190 * Math.cos(4 * P)
                + 0.128 * Math.sin(5 * P) - 0.034 * Math.cos(5 * P)
                - 0.038 * Math.sin(6 * P) + 0.031 * Math.cos(6 * P)
                + 0.020 * Math.sin(S - P) - 0.010 * Math.cos(S - P);

            latitude = -3.9082
                - 5.453 * Math.sin(P) - 14.975 * Math.cos(P)
                + 3.527 * Math.sin(2 * P) + 1.673 * Math.cos(2 * P)
                - 1.051 * Math.sin(3 * P) + 0.328 * Math.cos(3 * P)
                + 0.179 * Math.sin(4 * P) - 0.292 * Math.cos(4 * P)
                + 0.019 * Math.sin(5 * P) + 0.100 * Math.cos(5 * P)
                - 0.031 * Math.sin(6 * P) - 0.026 * Math.cos(6 * P)
                + 0.011 * Math.cos(S - P);

            r = 40.72
                + 6.68 * Math.sin(P) + 6.90 * Math.cos(P)
                - 1.18 * Math.sin(2 * P) - 0.03 * Math.cos(2 * P)
                + 0.15 * Math.sin(3 * P) - 0.14 * Math.cos(3 * P);
        }

        if (date.before(YEAR_2000)) {
            double correction = new ScientificNumber(3.82394, -5).value() * (365.2422 * (epoch() - 2000.0) - d());
            longitude += correction;
            if (!body.englishName().equals("Moon")) {
                latitude += correction;
            }
        }

        if (body.englishName().equals("Moon")) {
            OrbitalCalculator sun = new OrbitalCalculator(solarApi.bodyRegistry().search("Sun", true), solarApi);

            double Ms = sun.M();
            double Mm = M();
            double Nm = N();
            double ws = sun.w();
            double wm = w();
            double Ls = Ms + ws;
            double Lm = Mm + wm + Nm;
            double D = Lm - Ls;
            double F = Lm - Nm;

            longitude += -1.274 * Math.sin(Mm - 2 * D);
            longitude += 0.658 * Math.sin(2 * D);
            longitude += -0.186 * Math.sin(Ms);
            longitude += -0.059 * Math.sin(2 * Mm - 2 * D);
            longitude += -0.057 * Math.sin(Mm - 2 * D + Ms);
            longitude += 0.053 * Math.sin(Mm + 2 * D);
            longitude += 0.046 * Math.sin(2 * D - Ms);
            longitude += 0.041 * Math.sin(Mm - Ms);
            longitude += -0.035 * Math.sin(D);
            longitude += -0.031 * Math.sin(Mm + Ms);
            longitude += -0.015 * Math.sin(2 * F - 2 * D);
            longitude += 0.011 * Math.sin(Mm - 4 * D);

            latitude += -0.173 * Math.sin(F - 2 * D);
            latitude += -0.055 * Math.sin(Mm - F - 2 * D);
            latitude += -0.046 * Math.sin(Mm + F - 2 * D);
            latitude += 0.033 * Math.sin(F + 2 * D);
            latitude += 0.017 * Math.sin(2 * Mm + F);

            r += -0.58 * Math.cos(Mm - 2 * D);
            r += -0.46 * Math.cos(2 * D);
        } else if (body.englishName().equals("Jupiter") || body.englishName().equals("Saturn") || body.englishName().equals("Uranus")) {
            OrbitalCalculator jupiter = new OrbitalCalculator(solarApi.bodyRegistry().search("Jupiter", true), solarApi);
            OrbitalCalculator saturn = new OrbitalCalculator(solarApi.bodyRegistry().search("Saturn", true), solarApi);

            double Mj = jupiter.M();
            double Ms = saturn.M();

            if (body.englishName().equals("Jupiter")) {
                longitude += -0.332 * Math.sin((2 * Mj - 5 * Ms - 67.6));
                longitude += -0.056 * Math.sin(2 * Mj - 2 * Ms + 21);
                longitude += 0.042 * Math.sin(3 * Mj - 5 * Ms + 21);
                longitude += -0.036 * Math.sin(Mj - 2 * Ms);
                longitude += 0.022 * Math.cos(Mj - Ms);
                longitude += 0.023 * Math.sin(2 * Mj - 3 * Ms + 52);
                longitude += -0.016 * Math.sin(Mj - 5 * Ms - 69);
            } else if (body.englishName().equals("Saturn")) {
                longitude += 0.812 * Math.sin(2 * Mj - 5 * Ms - 67.6);
                longitude += -0.229 * Math.cos(2 * Mj - 4 * Ms - 2);
                longitude += 0.119 * Math.sin(Mj - 2 * Ms - 3);
                longitude += 0.046 * Math.sin(2 * Mj - 6 * Ms - 69);
                longitude += 0.014 * Math.sin(Mj - 3 * Ms + 32);

                latitude += -0.02 * Math.cos(2 * Mj - 4 * Ms - 2);
                latitude += 0.018 * Math.sin(2 * Mj - 6 * Ms - 49);
            } else if (body.englishName().equals("Uranus")) {
                double Mu = M();

                longitude += 0.04 * Math.sin(Ms - 2 * Mu + 6);
                longitude += 0.035 * Math.sin(Ms - 3 * Mu + 33);
                longitude += -0.015 * Math.sin(Mj - Mu + 20);
            }
        }
        eclipticCentric = new SolarSystemLocation(relativeTo, longitude, latitude, r);

        xh = r * Math.cos(longitude) * Math.cos(latitude);
        yh = r * Math.sin(longitude) * Math.cos(latitude);
        zh = r * Math.sin(latitude);

        helioCentric = new SolarSystemLocation(relativeTo, xh, yh, zh);

        double xs;
        double ys;

        if (body.englishName().equals("Moon")) {
            xs = xh;
            ys = yh;
        } else {
            xs = rs(solarApi) * Math.cos(longitude);
            ys = rs(solarApi) * Math.sin(longitude);
        }

        double xg = xh + xs;
        double yg = yh + ys;
        double zg = zh;


        if (body.englishName().equals("Earth")) {
            relativeTo = helioCentric;
        } else {
            relativeTo = earthLocation(solarApi);
        }
        geoCentric = new SolarSystemLocation(relativeTo, xg, yg, zg);

        double xe = xg;
        double ye = yg * Math.cos(ecl() - zg * Math.sin(ecl()));
        double ze = yg * Math.sin(ecl()) + zg * Math.cos(ecl());

        double RA = Math.atan2(ye, xe);
        double Dec = Math.atan2(ze, Math.sqrt(xe * xe + ye * ye));
        double rg = Math.sqrt(xe * xe + ye * ye + ze * ze);

        equatorial = new SolarSystemLocation(relativeTo, RA, Dec, rg);
    }

    public SolarSystemLocation helioCentric() {
        return helioCentric;
    }

    public SolarSystemLocation geoCentric() {
        return geoCentric;
    }

    public SolarSystemLocation equatorial() {
        return equatorial;
    }

    public SolarSystemLocation eclipticCentric() {
        return eclipticCentric;
    }

    public Double rs() {
        return rs;
    }

    public SolarSystemLocation earthLocation() {
        return earthLocation;
    }

    public SolarSystemLocation sunLocation() {
        return sunLocation;
    }
}
