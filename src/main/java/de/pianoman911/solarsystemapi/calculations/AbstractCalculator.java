package de.pianoman911.solarsystemapi.calculations;

import java.util.Date;

public abstract class AbstractCalculator {

    protected Date date = new Date(System.currentTimeMillis());

    public Date date() {
        return date;
    }

    public double kmPerHourToMeterPerSecond(double kmPerHour) {
        return kmPerHour / 3.6;
    }

    public double meterPerSecondToKmPerHour(double meterPerSecond) {
        return meterPerSecond * 3.6;
    }

    public double julianDate() {
        return date.getTime() / 86400000.0 + 2440587.5;
    }
}
