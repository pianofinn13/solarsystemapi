package de.pianoman911.solarsystemapi.calculations.nasahorizion.cache;

import de.pianoman911.solarsystemapi.util.SolarSystemLocation;
import de.pianoman911.solarsystemapi.util.StreamUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class ObjectData {

    private final String name;
    private final int id;
    private final Set<SolarSystemLocation> locations = new HashSet<>();

    public ObjectData(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public static ObjectData parse(BufferedInputStream in) {
        String name = StreamUtils.readString(in);
        int id = StreamUtils.readInt(in);

        ObjectData data = new ObjectData(name, id);

        int size = StreamUtils.readInt(in);
        for (int i = 0; i < size; i++) {
            SolarSystemLocation location = SolarSystemLocation.parse(in);
            data.addLocation(location);
        }

        return data;
    }

    public void addLocation(SolarSystemLocation location) {
        locations.add(location);
    }

    public void appendSerializer(BufferedOutputStream out) {
        StreamUtils.writeString(out, name);
        StreamUtils.writeInt(out, id);

        StreamUtils.writeInt(out, locations.size());
        for (SolarSystemLocation location : locations) {
            location.appendSerializer(out);
        }
    }

    public Queue<SolarSystemLocation> locations() {
        List<SolarSystemLocation> list = new ArrayList<>(locations);
        list.sort(Comparator.comparingLong(o -> o.date().getTime()));
        return new ArrayDeque<>(list);
    }

    public String name() {
        return name;
    }

    public int id() {
        return id;
    }
}
