package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import de.pianoman911.solarsystemapi.util.SolarSystemLocation;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MajorObjectDirectParser {

    public static MajorObjectInfo parseDirect(String name) {
        String url = JPLManager.JPL_URL + "?";
        url = RequestFormat.TEXT.append(url);
        url += "&" + "COMMAND='" + name + "'";
        url += "&" + "OBJ_DATA='YES'";
        url += "&" + "MAKE_EPHEM='NO'";

        String text = JPLManager.request(url).join();
        String[] lines = MajorObjectParser.lines(text);
        if (!MajorObjectParser.checkVersion(lines[0])) {
            throw new RuntimeException("Incompatible version of JPL Horizons");
        }

        if (lines[4].contains("Multiple major-bodies match string")) {
            return multiple(lines, name);
        } else {
            return single(lines);
        }
    }

    public static MajorObjectInfo multiple(String[] lines, String target) {
        int[] columnWidths = MajorObjectParser.columWidths(lines[7]);
        int matches = MajorObjectParser.count(lines[lines.length - 2]);
        int begin = 8;
        Map<String, MajorObjectInfo> objectInfoList = new HashMap<>();
        for (int i = 0; i < matches; i++) {
            String line = lines[begin + i];
            MajorObjectInfo object = MajorObjectParser.parseLine(line, columnWidths);
            objectInfoList.put(object.name(), object);
        }
        if (objectInfoList.isEmpty()) return null;
        String name = null;
        for (String s : objectInfoList.keySet()) {
            if (name == null) {
                name = s;
            } else {
                if (s.length() < name.length()) {
                    name = s;
                } else if (s.length() == name.length()) {
                    if (s.indexOf(target) < name.indexOf(target)) {
                        name = s;
                    }
                }
            }
        }

        return objectInfoList.get(name);
    }

    public static MajorObjectInfo single(String[] lines) {
        MajorObjectInfo object;
        if (lines[4].startsWith("JPL/HORIZONS")) {
            String line = lines[4].replaceAll("\\s+\\s+", " ");
            String[] split = line.split(" ");
            int id = Integer.parseInt(split[1]);
            StringBuilder name = new StringBuilder();
            for (int i = 2; i < split.length - 2; i++) {
                name.append(split[i]).append(" ");
            }
            return new MajorObjectInfo(id, name.toString(), "", "");

        } else if (lines[4].startsWith(" Revised:")) {
            String line = lines[4].replaceAll("\\s+\\s+", " ");
            String[] split = line.split(" ");
            StringBuilder name = new StringBuilder();
            for (int i = 5; i < split.length - 1; i++) {
                name.append(split[i]).append(" ");
            }
            object = new MajorObjectInfo(Integer.parseInt(split[split.length - 1]), name.toString(), "", "");
        } else {
            throw new RuntimeException("Unknown Single Major Object Type: \n" + String.join("\n", lines));
        }

        return object;
    }
}
