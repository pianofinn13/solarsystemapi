package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class JPLManager {

    public static final URL JPL_URL;
    private static final Map<HttpClient, Boolean> HTTP_CLIENTS = new ConcurrentHashMap<>();
    private static final HttpClient.Builder HTTP_CLIENT_BUILDER = HttpClient.newBuilder();
    private static final Executor EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();

    static {
        try {
            JPL_URL = new URL("https://ssd.jpl.nasa.gov/api/horizons.api");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("all")
    public static CompletableFuture<String> request(String request) {
        CompletableFuture<String> future = new CompletableFuture<>();
        EXECUTOR_SERVICE.execute(() -> {
            try {
                HttpRequest httpRequest = HttpRequest.newBuilder(new URI(request)).GET().build();
                HttpClient client = getHttpClient();
                client.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString()).whenComplete((r, e) -> {
                    future.complete(r.body());
                    HTTP_CLIENTS.put(client, false);
                });

            } catch (URISyntaxException e) {
                future.cancel(false);
                e.printStackTrace();
            }
        });
        return future;
    }

    private static synchronized HttpClient getHttpClient() {
        System.out.println("clients: " + HTTP_CLIENTS.size());
        for (Map.Entry<HttpClient, Boolean> clients : HTTP_CLIENTS.entrySet()) {
            if (!clients.getValue()) {
                HTTP_CLIENTS.put(clients.getKey(), true);
                return clients.getKey();
            }
        }
        HttpClient client = HttpClient.newHttpClient();
        HTTP_CLIENTS.put(client, true);
        return client;
    }
}
