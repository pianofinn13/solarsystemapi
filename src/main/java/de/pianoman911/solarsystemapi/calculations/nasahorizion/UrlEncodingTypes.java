package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import java.util.regex.Pattern;

public enum UrlEncodingTypes {

    LINE_FEED("\n", "%0A", Pattern.compile("\n")),
    SPACE(" ", "%20", Pattern.compile(" ")),
    HASH("#", "%23", Pattern.compile("#")),
    DOLLAR("$", "%24", Pattern.compile("\\$")),
    AMPERSENT("&", "%26", Pattern.compile("&")),
    PLUS("+", "%2B", Pattern.compile("\\+")),
    COMMA(",", "%2C", Pattern.compile(",")),
    SLASH("/", "%2F", Pattern.compile("/")),
    COLON(":", "%3A", Pattern.compile(":")),
    SEMICOLON(";", "%3B", Pattern.compile(";")),
    EQUAL("=", "%3D", Pattern.compile("=")),
    QUESTION_MARK("?", "%3F", Pattern.compile("\\?")),
    AT("@", "%40", Pattern.compile("@")),
    LEFT_SQUARE_BRACKET("[", "%5B", Pattern.compile("\\[")),
    RIGHT_SQUARE_BRACKET("]", "%5D", Pattern.compile("\\]")),
    ;

    private final String origin;
    private final String urlEncoding;
    private final Pattern pattern;

    UrlEncodingTypes(String origin, String urlEncoding, Pattern pattern) {
        this.origin = origin;
        this.urlEncoding = urlEncoding;
        this.pattern = pattern;
    }

    public static String encode(String origin,int startIndex) {
        String ignored = origin.substring(0, startIndex);
        String toEncode = origin.substring(startIndex);
        for (UrlEncodingTypes urlEncodingTypes : UrlEncodingTypes.values()) {
            toEncode = toEncode.replaceAll(urlEncodingTypes.pattern.pattern(), urlEncodingTypes.urlEncoding);
        }
        return ignored + toEncode;
    }


    public String origin() {
        return origin;
    }

    public String urlEncoding() {
        return urlEncoding;
    }
}
