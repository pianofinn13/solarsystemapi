package de.pianoman911.solarsystemapi.calculations.nasahorizion;

public enum EphemerisType {

    OBSERVER("observer"),
    VECTORS("vectors"),
    ELEMENTS("elements"),
    SPK("spk"),
    APPROACH("approach");

    private final String type;

    EphemerisType(String type) {
        this.type = type;
    }

    public String append(String url) {
        return url + "&" + "EPHEM_TYPE='" + type + "'" + "&"  + "MAKE_EPHEM='YES'";
    }
}
