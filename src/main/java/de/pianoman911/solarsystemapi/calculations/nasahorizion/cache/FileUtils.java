package de.pianoman911.solarsystemapi.calculations.nasahorizion.cache;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class FileUtils {

    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
    private static final int VERSION = 1;

    public static byte @NotNull [] compressToBytes(byte @NotNull [] bytes) {
        byte[] compressed = EMPTY_BYTE_ARRAY;

        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            try (GZIPOutputStream gzip = new GZIPOutputStream(output)) {
                gzip.write(bytes);
            } catch (Throwable ignored) {
                return compressed;
            }

            compressed = output.toByteArray();
        } catch (Throwable ignored) {
            return compressed;
        }

        return compressed;
    }

    public static byte @NotNull [] decompressBytes(byte @NotNull [] bytes) {
        byte[] decompressed = EMPTY_BYTE_ARRAY;

        try (ByteArrayInputStream input = new ByteArrayInputStream(bytes); GZIPInputStream gzip = new GZIPInputStream(input)) {
            decompressed = gzip.readAllBytes();
        } catch (Throwable ignored) {
            return decompressed;
        }

        return decompressed;
    }

    public static void save(File file, Container container) {
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            try (BufferedOutputStream bufferedOutput = new BufferedOutputStream(output)) {
                container.appendSerializer(bufferedOutput);
            }

            byte[] compressed = compressToBytes(output.toByteArray());

            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(VERSION);
                fos.write(compressed);
            }
        } catch (Throwable ignored) {
        }
    }

    public static Container load(File file) {
        try (FileInputStream fis = new FileInputStream(file)) {
            int version = fis.read();
            if (version != VERSION) {
                throw new VersionNotSupportedException(VERSION, version);
            }
            byte[] compressed = fis.readAllBytes();
            byte[] decompressed = decompressBytes(compressed);

            try (ByteArrayInputStream input = new ByteArrayInputStream(decompressed); BufferedInputStream bufferedInput = new BufferedInputStream(input)) {
                return Container.parse(bufferedInput);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
