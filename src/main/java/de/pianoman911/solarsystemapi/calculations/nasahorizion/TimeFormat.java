package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFormat {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd" + UrlEncodingTypes.SPACE.urlEncoding() +"HH:mm:ss");

    public static String startTime(String url, Date date) {
        return url + "&" + "START_TIME='" + DATE_FORMAT.format(date) + "'";
    }

    public static String stopTime(String url, Date date) {
        return url + "&" + "STOP_TIME='" + DATE_FORMAT.format(date) + "'";
    }

    public static String step(String url, int step, StepUnit unit) {
        return url + "&" + "STEP_SIZE=" + step + unit.value();
    }
}
