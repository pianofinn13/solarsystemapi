package de.pianoman911.solarsystemapi.calculations.nasahorizion;

public enum RequestFormat {

    JSON("json"),
    TEXT("text");

    private final String format;

    RequestFormat(String format) {
        this.format = format;
    }

    public String append(String url) {
        return url + "format=" + format;
    }

    public String value() {
        return format;
    }
}
