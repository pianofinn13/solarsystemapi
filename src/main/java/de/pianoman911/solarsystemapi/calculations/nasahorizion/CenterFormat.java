package de.pianoman911.solarsystemapi.calculations.nasahorizion;

public class CenterFormat {

    public static String at(String url, String obj, String center) {
        return url + "&" + "CENTER='" + obj + "@" + center + "'";
    }

    public static String sun(String url) {
        return url + "&" + "CENTER='@sun'";
    }
}
