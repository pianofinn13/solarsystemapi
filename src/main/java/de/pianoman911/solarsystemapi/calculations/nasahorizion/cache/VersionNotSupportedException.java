package de.pianoman911.solarsystemapi.calculations.nasahorizion.cache;

public class VersionNotSupportedException extends Exception {


    public VersionNotSupportedException(int need, int have) {
        super("Version " + need + " is not supported. Have " + have + ". Please update your version of the cache." );
    }
}
