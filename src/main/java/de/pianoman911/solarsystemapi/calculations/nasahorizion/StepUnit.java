package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public enum StepUnit {

    SECOND("s", TimeUnit.SECONDS::toMillis),
    MINUTE("m", TimeUnit.MINUTES::toMillis),
    HOUR("h", TimeUnit.HOURS::toMillis),
    DAY("d", TimeUnit.DAYS::toMillis),
    MONTH("M", stamp -> stamp * 24 * 60 * 60 * 1000),
    YEAR("y", stamp -> stamp * 24 * 60 * 60 * 1000 * 365);

    private final String unit;
    private final Function<Long, Long> toMillis;

    StepUnit(String unit, Function<Long, Long> toMillis) {
        this.unit = unit;
        this.toMillis = toMillis;
    }

    public String value() {
        return unit;
    }

    public long toMillis(long stamp) {
        return toMillis.apply(stamp);
    }
}
