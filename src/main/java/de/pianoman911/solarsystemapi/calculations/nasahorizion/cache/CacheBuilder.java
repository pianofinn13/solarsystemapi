package de.pianoman911.solarsystemapi.calculations.nasahorizion.cache;

import de.pianoman911.solarsystemapi.calculations.nasahorizion.MajorObjectInfo;
import de.pianoman911.solarsystemapi.calculations.nasahorizion.StepUnit;
import de.pianoman911.solarsystemapi.util.SolarSystemLocation;

import java.util.Date;
import java.util.Set;

public class CacheBuilder {

    private static final int MAX_COUNT_PER_REQUEST = 75000;

    public static Container buildCache(Set<MajorObjectInfo> majorObjects, Date from, Date to, int stepSize, StepUnit stepUnit) {
        if (from.getTime() >= to.getTime()) {
            throw new IllegalArgumentException("from must be before to");
        }
        Container container = new Container();

        int count = (int) ((to.getTime() - from.getTime()) / stepUnit.toMillis(stepSize));

        if (count > MAX_COUNT_PER_REQUEST) {
            for (int i = 0; i < count / MAX_COUNT_PER_REQUEST + 1; i++) {
                long offset = stepUnit.toMillis(stepSize) * i * MAX_COUNT_PER_REQUEST;
                Date from2 = new Date(from.getTime() + offset);
                Date to2 = new Date(from2.getTime() + stepUnit.toMillis(stepSize) * MAX_COUNT_PER_REQUEST);
                if (to2.getTime() > to.getTime()) to2 = to;

                for (MajorObjectInfo majorObject : majorObjects) {
                    ObjectData objectData = new ObjectData(majorObject.name(), majorObject.id());
                    container.addObject(objectData);
                    for (SolarSystemLocation location : majorObject.locations(from2, to2, stepSize, stepUnit)) {
                        objectData.addLocation(location);
                    }
                }
            }
        } else {
            for (MajorObjectInfo majorObject : majorObjects) {
                System.out.println("Building cache for " + majorObject.name());
                ObjectData objectData = new ObjectData(majorObject.name(), majorObject.id());
                container.addObject(objectData);
                for (SolarSystemLocation location : majorObject.locations(from, to, stepSize, stepUnit)) {
                    objectData.addLocation(location);
                }
            }
        }

        return container;
    }
}
