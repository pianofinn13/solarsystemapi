package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import de.pianoman911.solarsystemapi.util.SolarSystemLocation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public record MajorObjectInfo(int id, String name, String designation, String alias) {

    @Override
    public String toString() {
        return "MajorObject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", designation='" + designation + '\'' +
                ", alias='" + alias + '\'' +
                '}';
    }

    public List<SolarSystemLocation> locations(Date date, Date date2, int step, StepUnit unit) {
        if (date.getTime() == date2.getTime()) {
            throw new IllegalArgumentException("Start and stop time are the same");
        }
        String url = JPLManager.JPL_URL + "?";
        url = RequestFormat.TEXT.append(url);
        url += "&" + "COMMAND='" + id + "'";
        url = EphemerisType.VECTORS.append(url);
        url = TimeFormat.startTime(url, date);
        url = TimeFormat.stopTime(url, date2);
        url = TimeFormat.step(url, step, unit);
        url = CenterFormat.sun(url);

        String text = JPLManager.request(url).join();
        String[] lines = lines(text);

        int i = 0;
        for (; i < lines.length; i++) {
            if (lines[i].equals("$$SOE")) {
                break;
            }
        }
        List<SolarSystemLocation> locations = new ArrayList<>();
        i += 2;
        Date current = new Date(date.getTime());
        while (i < lines.length && !lines[i].equals("$$EOE")) {
            if (lines[i].startsWith(" X")) {
                double[] coordinates = parseLine(lines[i]);
                locations.add(new SolarSystemLocation(coordinates[0], coordinates[1], coordinates[2], new Date(current.getTime())));

                current.setTime(current.getTime() + unit.toMillis(step));
            }
            i++;
        }

        return locations;
    }

    public SolarSystemLocation location(Date date) {
        Date other = new Date(date.getTime() + 86400000);
        return locations(date, other, 1, StepUnit.DAY).get(0);
    }

    private String[] lines(String text) {
        return text.split("\n");
    }

    private double[] parseLine(String line) {
        double x = Double.parseDouble(line.substring(4, line.indexOf("Y")));
        double y = Double.parseDouble(line.substring(line.indexOf("Y") + 3, line.indexOf("Z")));
        double z = Double.parseDouble(line.substring(line.indexOf("Z") + 3));

        return new double[]{x, y, z};
    }
}