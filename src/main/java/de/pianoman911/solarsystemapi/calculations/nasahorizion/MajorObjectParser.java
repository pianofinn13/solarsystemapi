package de.pianoman911.solarsystemapi.calculations.nasahorizion;

import java.util.HashMap;
import java.util.Map;

public class MajorObjectParser {

    private static final String COMPATIBLE_VERSION = "1.1";

    public static Map<Integer, MajorObjectInfo> parseObjects() {
        String url = JPLManager.JPL_URL + "?";
        url = RequestFormat.TEXT.append(url);
        url += "&" + "COMMAND='A'";
        String text = JPLManager.request(url).join();

        String[] lines = lines(text);
        if (!checkVersion(lines[0])) {
            throw new RuntimeException("Incompatible version of JPL Horizons");
        }
        int[] columnWidths = columWidths(lines[7]);
        int matches = count(lines[lines.length - 2]);

        int begin = 8;
        Map<Integer, MajorObjectInfo> objects = new HashMap<>();
        for (int i = 0; i < matches; i++) {
            MajorObjectInfo object = parseLine(lines[begin + i], columnWidths);
            objects.put(object.id(), object);
        }

        return objects;
    }

    protected static String[] lines(String text) {
        return text.split("\n");
    }

    protected static MajorObjectInfo parseLine(String text, int[] columnWidths) {
        int id = Integer.parseInt(text.substring(2, 2 + columnWidths[0]).replaceAll("\\s+", ""));
        String name = text.substring(2 + columnWidths[0], 2 + columnWidths[0] + columnWidths[1]).replaceAll("\\s+\\s+", "");
        String designation = text.substring(2 + columnWidths[0] + columnWidths[1], 2 + columnWidths[0] + columnWidths[1] + columnWidths[2]).replaceAll("\\s+\\s+", "");
        String alias = text.substring(2 + columnWidths[0] + columnWidths[1] + columnWidths[2], 2 + columnWidths[0] + columnWidths[1] + columnWidths[2] + columnWidths[3]).replaceAll("\\s+\\s+", "");

        return new MajorObjectInfo(id, name, designation, alias);
    }

    protected static int[] columWidths(String text) {
        String[] split = text.replaceAll("[ ]+", " ").split(" ");
        int[] chars = new int[split.length - 1];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = split[i + 1].length();
        }
        return chars;
    }

    protected static boolean checkVersion(String text) {
        String[] split = text.split(" ");
        if (split.length == 3) {
            return split[2].equals(COMPATIBLE_VERSION);
        }
        return false;
    }

    protected static int count(String line) {
        String[] split = line.replaceAll("\\s+\\s+", " ").split(" ");
        return Integer.parseInt(split[5].substring(0, split[5].length() - 1));
    }
}
