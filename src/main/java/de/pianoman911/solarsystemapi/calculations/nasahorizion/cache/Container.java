package de.pianoman911.solarsystemapi.calculations.nasahorizion.cache;

import de.pianoman911.solarsystemapi.util.StreamUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

public class Container {

    private final Set<ObjectData> objects = new HashSet<>();

    public static Container parse(BufferedInputStream in) {
        int size = StreamUtils.readInt(in);
        Container container = new Container();
        for (int i = 0; i < size; i++) {
            container.addObject(ObjectData.parse(in));
        }
        return container;
    }

    public void addObject(ObjectData object) {
        objects.add(object);
    }

    public void appendSerializer(BufferedOutputStream out) {
        StreamUtils.writeInt(out, objects.size());
        for (ObjectData object : objects) {
            object.appendSerializer(out);
        }
    }

    public int size() {
        return objects.size();
    }

    public Set<ObjectData> objects() {
        return objects;
    }
}
