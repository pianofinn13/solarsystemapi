package de.pianoman911.solarsystemapi;

public enum BodyType {

    PLANET("Planet"),
    MOON("Moon"),
    ASTEROID("Asteroid"),
    DWARF_PLANET("Dwarf Planet"),
    COMET("Comet"),
    STAR("Star");

    private final String name;

    BodyType(String name) {
        this.name = name;
    }

    public static BodyType parse(String value) {
        for (BodyType type : values()) {
            if (type.name.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown body type: " + value);
    }

    public String jName() {
        return name;
    }
}
