package de.pianoman911.solarsystemapi;

import de.pianoman911.solarsystemapi.calculations.nasahorizion.MajorObjectDirectParser;
import de.pianoman911.solarsystemapi.calculations.nasahorizion.StepUnit;

import java.util.Date;

public class Test {

    public static void main(String[] args) {
        SolarApi solarApi = new SolarApi();
        solarApi.buildAll();

        Date date = new Date();
        Date date2 = new Date(date.getTime() + 86400000);

        System.out.println(solarApi.bodyRegistry().searchMajorObject("Earth", true).locations(date, date2, 1, StepUnit.DAY));
    }
}
