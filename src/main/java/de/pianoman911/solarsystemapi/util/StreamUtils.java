package de.pianoman911.solarsystemapi.util;

import java.io.InputStream;
import java.io.OutputStream;

public class StreamUtils {

    public static void writeShort(OutputStream out, short value) {
        try {
            out.write(value >> 8);
            out.write(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static short readShort(InputStream in) {
        try {
            int b1 = in.read();
            int b2 = in.read();
            return (short) ((b1 << 8) | b2);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeInt(OutputStream out, int value) {
        try {
            out.write(value >> 24);
            out.write(value >> 16);
            out.write(value >> 8);
            out.write(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int readInt(InputStream in) {
        try {
            int b1 = in.read();
            int b2 = in.read();
            int b3 = in.read();
            int b4 = in.read();
            return (b1 << 24) | (b2 << 16) | (b3 << 8) | b4;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeLong(OutputStream out, long value) {
        try {
            out.write((int) (value >> 56));
            out.write((int) (value >> 48));
            out.write((int) (value >> 40));
            out.write((int) (value >> 32));
            out.write((int) (value >> 24));
            out.write((int) (value >> 16));
            out.write((int) (value >> 8));
            out.write((int) value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long readLong(InputStream in) {
        try {
            long b1 = in.read();
            long b2 = in.read();
            long b3 = in.read();
            long b4 = in.read();
            long b5 = in.read();
            long b6 = in.read();
            long b7 = in.read();
            long b8 = in.read();
            return (b1 << 56) | (b2 << 48) | (b3 << 40) | (b4 << 32) | (b5 << 24) | (b6 << 16) | (b7 << 8) | b8;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeFloat(OutputStream out, float value) {
        try {
            writeInt(out, Float.floatToIntBits(value));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static float readFloat(InputStream in) {
        try {
            return Float.intBitsToFloat(readInt(in));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeDouble(OutputStream out, double value) {
        try {
            writeLong(out, Double.doubleToLongBits(value));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double readDouble(InputStream in) {
        try {
            return Double.longBitsToDouble(readLong(in));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeString(OutputStream out, String value) {
        try {
            byte[] bytes = value.getBytes();
            writeInt(out, bytes.length);
            out.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readString(InputStream in) {
        try {
            int length = readInt(in);
            byte[] bytes = new byte[length];
            in.read(bytes);
            return new String(bytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeBoolean(OutputStream out, boolean value) {
        try {
            out.write(value ? 1 : 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean readBoolean(InputStream in) {
        try {
            return in.read() == 1;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
