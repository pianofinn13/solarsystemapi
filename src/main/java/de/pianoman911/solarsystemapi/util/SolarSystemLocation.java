package de.pianoman911.solarsystemapi.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Date;

public class SolarSystemLocation {

    public static final SolarSystemLocation MIDDLE = new SolarSystemLocation();
    private final SolarSystemLocation relativeTo;

    private final Date date;
    private double x;
    private double y;
    private double z;

    private SolarSystemLocation() {
        this(null, 0, 0, 0, new Date());
    }

    public SolarSystemLocation(double x, double y, double z) {
        this(MIDDLE, x, y, z, new Date());
    }

    public SolarSystemLocation(double x, double y, double z, Date date) {
        this(MIDDLE, x, y, z, date);
    }
    public SolarSystemLocation(SolarSystemLocation relativeTo, double x, double y, double z) {
        this(relativeTo, x, y, z, new Date());
    }

    public SolarSystemLocation(SolarSystemLocation relativeTo, double x, double y, double z, Date date) {
        this.relativeTo = relativeTo;
        this.x = x;
        this.y = y;
        this.z = z;
        this.date = date;
    }

    public static SolarSystemLocation parse(BufferedInputStream in) {
        double x = StreamUtils.readDouble(in);
        double y = StreamUtils.readDouble(in);
        double z = StreamUtils.readDouble(in);
        Date date = new Date(StreamUtils.readLong(in));
        return new SolarSystemLocation(MIDDLE, x, y, z, date);
    }

    public SolarSystemLocation relativeTo() {
        return relativeTo;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double z() {
        return z;
    }

    public Date date() {
        return date;
    }

    public double distance(SolarSystemLocation other) {
        return Math.sqrt(Math.pow(x - other.x(), 2) + Math.pow(y - other.y(), 2) + Math.pow(z - other.z(), 2));
    }

    @Override
    public String toString() {
        return "SolarSystemLocation{" +
                "relativeTo=" + relativeTo +
                ", date=" + date +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public void appendSerializer(BufferedOutputStream out) {
        StreamUtils.writeDouble(out, x);
        StreamUtils.writeDouble(out, y);
        StreamUtils.writeDouble(out, z);
        StreamUtils.writeLong(out, date.getTime());
    }
}
