package de.pianoman911.solarsystemapi.util.projection;

public class SphericalProjection {

    private double rightAscension;
    private double declination;
    private double distance;

    public SphericalProjection(double rightAscension, double declination, double distance) {
        this.rightAscension = rightAscension;
        this.declination = declination;
        this.distance = distance;
    }

    public double rightAscension() {
        return rightAscension;
    }

    public double declination() {
        return declination;
    }

    public double distance() {
        return distance;
    }

    public void rightAscension(double rightAscension) {
        this.rightAscension = rightAscension;
    }

    public void declination(double declination) {
        this.declination = declination;
    }

    public void distance(double distance) {
        this.distance = distance;
    }
}
