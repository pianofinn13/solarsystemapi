package de.pianoman911.solarsystemapi.util.projection;

public class RectangularProjection {

    private double x;
    private double y;
    private double z;

    public RectangularProjection(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double z() {
        return z;
    }

    public void x(double x) {
        this.x = x;
    }

    public void y(double y) {
        this.y = y;
    }

    public void z(double z) {
        this.z = z;
    }
}
