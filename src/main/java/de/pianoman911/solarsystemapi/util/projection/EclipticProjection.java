package de.pianoman911.solarsystemapi.util.projection;

public class EclipticProjection {

    private double x;
    private double y;
    private double z;
    private double obliquity;

    public EclipticProjection(double x, double y, double z, double obliquity) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.obliquity = obliquity;
    }

    public double x() {
        return x;
    }

    public void x(double x) {
        this.x = x;
    }

    public double y() {
        return y;
    }

    public void y(double y) {
        this.y = y;
    }

    public double z() {
        return z;
    }

    public void z(double z) {
        this.z = z;
    }

    public double obliquity() {
        return obliquity;
    }

    public void obliquity(double obliquity) {
        this.obliquity = obliquity;
    }
}
