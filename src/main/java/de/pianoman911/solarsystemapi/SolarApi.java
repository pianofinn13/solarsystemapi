package de.pianoman911.solarsystemapi;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.pianoman911.solarsystemapi.calculations.nasahorizion.MajorObjectInfo;
import de.pianoman911.solarsystemapi.calculations.nasahorizion.MajorObjectParser;
import de.pianoman911.solarsystemapi.objects.Body;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SolarApi {

    /*
    https://api.le-systeme-solaire.net/en/
     */
    private final HttpClient client = HttpClient.newHttpClient();
    private final Gson gson = new Gson();
    private final BodyRegistry bodyRegistry = new BodyRegistry();
    private JsonObject solarSystem;

    public void requestSolarSystem() {
        try {
            HttpRequest request = HttpRequest.newBuilder(new URI("https://api.le-systeme-solaire.net/rest.php/bodies")).build();
            HttpResponse<String> json = client.send(request, HttpResponse.BodyHandlers.ofString());
            solarSystem = gson.fromJson(json.body(), JsonObject.class);

        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void buildSolarSystem() {
        for (JsonElement bodies : solarSystem.get("bodies").getAsJsonArray()) {
            bodyRegistry.registerBody(new Body(this, bodies.getAsJsonObject()));
        }
    }

    public void buildNasaJspObjects() {
        for (MajorObjectInfo value : MajorObjectParser.parseObjects().values()) {
            bodyRegistry.registerMajorObject(value);
        }
    }

    public void buildAll() {
        requestSolarSystem();
        buildSolarSystem();
        buildNasaJspObjects();
    }

    public JsonObject solarSystem() {
        return solarSystem;
    }

    public BodyRegistry bodyRegistry() {
        return bodyRegistry;
    }
}
