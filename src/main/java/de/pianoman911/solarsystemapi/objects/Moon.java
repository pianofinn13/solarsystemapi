package de.pianoman911.solarsystemapi.objects;

import com.google.gson.JsonObject;
import de.pianoman911.solarsystemapi.SolarApi;

public class Moon {

    private final SolarApi solarApi;
    private String moon;
    private String rel;

    public Moon(SolarApi solarApi, JsonObject obj) {
        this.solarApi = solarApi;
        this.moon = obj.get("moon").getAsString();
        this.rel = obj.get("rel").getAsString();
    }

    public Body body() {
        return solarApi.bodyRegistry().body(rel);
    }

    public SolarApi solarApi() {
        return solarApi;
    }

    public String moon() {
        return moon;
    }

    public String rel() {
        return rel;
    }

    @Override
    public String toString() {
        return "objects.Moon{" +
            "solarApi=" + solarApi +
            ", moon='" + moon + '\'' +
            ", rel='" + rel + '\'' +
            '}';
    }
}
