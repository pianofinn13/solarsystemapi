package de.pianoman911.solarsystemapi.objects;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.pianoman911.solarsystemapi.BodyType;
import de.pianoman911.solarsystemapi.ScientificNumber;
import de.pianoman911.solarsystemapi.SolarApi;

import java.util.ArrayList;
import java.util.List;

public class Body {

    private final SolarApi solarApi;
    private String id;
    private String name;
    private String englishName;
    private boolean isPlanet;
    private List<Moon> moons = new ArrayList<>();
    private long semimajorAxis;
    private long perihelion;
    private long aphelion;
    private double eccentricity;
    private double inclination;
    private ScientificNumber mass;
    private ScientificNumber vol;
    private double density;
    private double gravity;
    private long escape;
    private double meanRadius;
    private double equaRadius;
    private double polarRadius;
    private double flattening;
    private String dimension;
    private double sideralOrbit;
    private double sideralRotation;
    private AroundPlanet arroundPlanet;
    private String discoveredBy;
    private String discoveryDate;
    private String alternativeName;
    private double axialTilt;
    private double avgTemp;
    private double mainAnomaly;
    private double argPeriapsis;
    private double longAscNode;
    private BodyType bodyType;
    private String rel;

    public Body(SolarApi solarApi, JsonObject obj) {
        this.solarApi = solarApi;
        this.id = obj.get("id").getAsString();
        this.name = obj.get("name").getAsString();
        this.englishName = obj.get("englishName").getAsString();
        this.isPlanet = obj.get("isPlanet").getAsBoolean();
        if (!obj.get("moons").isJsonNull()) {
            for (JsonElement moon : obj.get("moons").getAsJsonArray()) {
                this.moons.add(new Moon(solarApi, moon.getAsJsonObject()));
            }
        }
        this.semimajorAxis = obj.get("semimajorAxis").getAsLong();
        this.perihelion = obj.get("perihelion").getAsLong();
        this.aphelion = obj.get("aphelion").getAsLong();
        this.eccentricity = obj.get("eccentricity").getAsDouble();
        this.inclination = obj.get("inclination").getAsDouble();
        if (!obj.get("mass").isJsonNull()) {
            JsonObject mass = obj.get("mass").getAsJsonObject();
            this.mass = new ScientificNumber(mass.get("massValue").getAsDouble(), mass.get("massExponent").getAsInt());
        }
        if (!obj.get("vol").isJsonNull()) {
            JsonObject vol = obj.get("vol").getAsJsonObject();
            this.vol = new ScientificNumber(vol.get("volValue").getAsDouble(), vol.get("volExponent").getAsInt());
        }
        this.density = obj.get("density").getAsDouble();
        this.gravity = obj.get("gravity").getAsDouble();
        this.escape = obj.get("escape").getAsLong();
        this.meanRadius = obj.get("meanRadius").getAsDouble();
        this.equaRadius = obj.get("equaRadius").getAsDouble();
        this.polarRadius = obj.get("polarRadius").getAsDouble();
        this.flattening = obj.get("flattening").getAsDouble();
        this.dimension = obj.get("dimension").getAsString();
        this.sideralOrbit = obj.get("sideralOrbit").getAsDouble();
        this.sideralRotation = obj.get("sideralRotation").getAsDouble();
        if (!obj.get("aroundPlanet").isJsonNull()) {
            this.arroundPlanet = new AroundPlanet(solarApi, obj.get("aroundPlanet").getAsJsonObject());
        }
        this.discoveredBy = obj.get("discoveredBy").getAsString();
        this.discoveryDate = obj.get("discoveryDate").getAsString();
        this.alternativeName = obj.get("alternativeName").getAsString();
        this.axialTilt = obj.get("axialTilt").getAsDouble();
        this.avgTemp = obj.get("avgTemp").getAsDouble();
        this.mainAnomaly = obj.get("mainAnomaly").getAsDouble();
        this.argPeriapsis = obj.get("argPeriapsis").getAsDouble();
        this.longAscNode = obj.get("longAscNode").getAsDouble();
        this.bodyType = BodyType.parse(obj.get("bodyType").getAsString());
        this.rel = obj.get("rel").getAsString();
    }

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String englishName() {
        return englishName;
    }

    public boolean isPlanet() {
        return isPlanet;
    }

    public List<Moon> moons() {
        return moons;
    }

    public long semimajorAxis() {
        return semimajorAxis;
    }

    public long perihelion() {
        return perihelion;
    }

    public long aphelion() {
        return aphelion;
    }

    public double eccentricity() {
        return eccentricity;
    }

    public double inclination() {
        return inclination;
    }

    public ScientificNumber mass() {
        return mass;
    }

    public ScientificNumber vol() {
        return vol;
    }

    public double density() {
        return density;
    }

    public double gravity() {
        return gravity;
    }

    public long escape() {
        return escape;
    }

    public double meanRadius() {
        return meanRadius;
    }

    public double equaRadius() {
        return equaRadius;
    }

    public double polarRadius() {
        return polarRadius;
    }

    public double flattening() {
        return flattening;
    }

    public String dimension() {
        return dimension;
    }

    public double sideralOrbit() {
        return sideralOrbit;
    }

    public double sideralRotation() {
        return sideralRotation;
    }

    public AroundPlanet arroundPlanet() {
        return arroundPlanet;
    }

    public String discoveredBy() {
        return discoveredBy;
    }

    public String discoveryDate() {
        return discoveryDate;
    }

    public String alternativeName() {
        return alternativeName;
    }

    public double axialTilt() {
        return axialTilt;
    }

    public double avgTemp() {
        return avgTemp;
    }

    public double meanAnomaly() {
        return mainAnomaly;
    }

    public double argPeriapsis() {
        return argPeriapsis;
    }

    public double longAscNode() {
        return longAscNode;
    }

    public BodyType bodyType() {
        return bodyType;
    }

    public String rel() {
        return rel;
    }

    @Override
    public String toString() {
        return "objects.Body{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", englishName='" + englishName + '\'' + ", isPlanet=" + isPlanet + ", moons=" + moons + ", semimajorAxis=" + semimajorAxis + ", perihelion=" + perihelion + ", aphelion=" + aphelion + ", eccentricity=" + eccentricity + ", inclination=" + inclination + ", mass=" + mass + ", vol=" + vol + ", density=" + density + ", gravity=" + gravity + ", escape=" + escape + ", meanRadius=" + meanRadius + ", equaRadius=" + equaRadius + ", polarRadius=" + polarRadius + ", flattening=" + flattening + ", dimension='" + dimension + '\'' + ", sideralOrbit=" + sideralOrbit + ", sideralRotation=" + sideralRotation + ", arroundPlanet=" + arroundPlanet + ", discoveredBy='" + discoveredBy + '\'' + ", discoveryDate='" + discoveryDate + '\'' + ", alternativeName='" + alternativeName + '\'' + ", axialTilt=" + axialTilt + ", avgTemp=" + avgTemp + ", mainAnomaly=" + mainAnomaly + ", argPeriapsis=" + argPeriapsis + ", longAscNode=" + longAscNode + ", bodyType=" + bodyType + ", rel='" + rel + '\'' + '}';
    }
}
