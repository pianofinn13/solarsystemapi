package de.pianoman911.solarsystemapi.objects;

import com.google.gson.JsonObject;
import de.pianoman911.solarsystemapi.SolarApi;

public class AroundPlanet {

    private final SolarApi solarApi;
    private String planet;
    private String rel;

    public AroundPlanet(SolarApi solarApi, JsonObject obj) {
        this.solarApi = solarApi;
        this.planet = obj.get("planet").getAsString();
        this.rel = obj.get("rel").getAsString();
    }

    public Body body() {
        return solarApi.bodyRegistry().body(rel);
    }

    public SolarApi solarApi() {
        return solarApi;
    }

    public String planet() {
        return planet;
    }

    public String rel() {
        return rel;
    }

    @Override
    public String toString() {
        return "objects.AroundPlanet{" +
            "solarApi=" + solarApi +
            ", planet='" + planet + '\'' +
            ", rel='" + rel + '\'' +
            '}';
    }
}
