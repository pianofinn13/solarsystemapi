plugins {
    id("java-library")
    id("maven-publish")
}

group = "de.pianoman911"
version = "1.2.1"

repositories {
    mavenCentral()
}

dependencies {
    api("com.google.code.gson:gson:2.9.0")
    api("org.jetbrains:annotations:23.0.0")
}

publishing {
    publications.create<MavenPublication>("maven") {
        artifactId = project.name.toLowerCase()
        from(components["java"])
    }

    repositories.maven("https://gitlab.com/api/v4/projects/37339111/packages/maven/") {
        authentication { create<HttpHeaderAuthentication>("header") }
        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = file("${project.projectDir}/token.txt").readText()
        }
    }
}
